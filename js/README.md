Widget to visualise X3D cad models

Package Install
---------------

**Prerequisites**
- [node](http://nodejs.org/)

```bash
npm install --save partbrowser
```
