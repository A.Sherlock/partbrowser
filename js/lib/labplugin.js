var plugin = require('./index');
var base = require('@jupyter-widgets/base');

module.exports = {
  id: 'partbrowser',
  requires: [base.IJupyterWidgetRegistry],
  activate: function(app, widgets) {
      widgets.registerWidget({
          name: 'partbrowser',
          version: plugin.version,
          exports: plugin
      });
  },
  autoStart: true
};

